psql
CREATE USER formation;
ALTER ROLE formation WITH CREATEDB;
CREATE DATABASE formation OWNER formation;
ALTER USER formation WITH ENCRYPTED PASSWORD 'formation';
\q
exit
./mvnw spring-boot:run
