# TP_J2E Paul Mazeran : Gestionnaire d'évènements

L'objectif du TP était de développer un site web de gestion d'évènements en utilisant Spring MVC.

## Lancer le projet

Afin de lancer la projet, vous allez devoir effectué les tâches suivantes :

  1. Créer un dossier en local et ouvrir un terminal dans le dossier.
  2. Exécuter les commandes suivantes : 

    $ git clone https://gitlab.com/paulmazeran/tp_j2e_mazeran.git
    $ cd tp_j2e_mazeran
    $ sudo -i -u postgres
    $ psql
    $ CREATE USER formation;
    $ ALTER ROLE formation WITH CREATEDB;
    $ CREATE DATABASE formation OWNER formation;
    $ ALTER USER formation WITH ENCRYPTED PASSWORD 'formation';
    $ \q
    $ exit
    $ ./mvnw spring-boot:run

 3. Allez à l'adresse localhost:8080 suivante en cliquant sur le lien : [lien](https:localhost:8080%20dans%20votre%20navigateur.).
 4. Vous pouvez maintenant naviguer sur le gestionnaire d'évènements.

### Prerequis

Jva8 doit être installé sur votre machine : 

    $ sudo apt install openjdk8-jdk

PosgreSQL doit être installé sur votre machine :

    $ sudo apt install postgres

#### Présentation du site web 

Sur le site, il est possible :

 - D'ajouter des évènements (bouton "Ajouter un évènement")
 - D'ajouter des participants (bouton "Ajouter un participant")
 - De voir la liste des évènements (bouton "Liste des évènement")
 - De voir la liste des participants (bouton "Liste des participants")
