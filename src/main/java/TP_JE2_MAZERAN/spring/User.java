/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TP_JE2_MAZERAN.spring;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity // This tells Hibernate to make a table out of this class
@Table(name="Users")
public class User {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Integer id;
  
  private String eventname;
  
  private String firstname;
  
  private String lastname;

  private String email;
  
//  public User(String firstName, String lastName, String email) {
//      this.firstname = firstName;
//      this.lastname = lastName;
//      this.email = email;
//  }


  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String name) {
    this.firstname = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

public String getLastname() {
	return lastname;
}

public void setLastname(String lastname) {
	this.lastname = lastname;
}



public String getEventname() {
	return eventname;
}

public void setEventname(String eventname) {
	this.eventname = eventname;
}
}