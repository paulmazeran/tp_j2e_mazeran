package TP_JE2_MAZERAN.spring;

import org.springframework.data.repository.CrudRepository;

import TP_JE2_MAZERAN.spring.Event;
// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface EventRepository extends CrudRepository<Event, Integer> {

}