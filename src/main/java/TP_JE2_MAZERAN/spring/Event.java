package TP_JE2_MAZERAN.spring;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity // This tells Hibernate to make a table out of this class
@Table(name="Events")
public class Event {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Integer id;
  
  private String name;
  
  private String sport;

  private String date;

public Integer getId() {
	return id;
}

public void setId(Integer id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getSport() {
	return sport;
}

public void setSport(String sport) {
	this.sport = sport;
}

public String getDate() {
	return date;
}

public void setDate(String date) {
	this.date = date;
}
  
}